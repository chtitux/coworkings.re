---
coarse_location: Front de mer, Saint-Leu
region: Ouest
location:
  title: Médiathèque Baguett'
  latitude: -21.165748623933396
  longitude: 55.28646262575122
title: Médiathèque Baguett'
permalink:  /espace-adapte/saint-leu/médiathèque-baguett/
logo: assets/espaces-adaptes/logo-saint-leu.png
website: https://www.mediatheque-saintleu.re/le-reseau/mediatheque-baguett
phone: '+262 262 45 79 00'
address: rue Waldeck Rousseau, 97436 Saint-Leu
---
La **Médiatèque Baguett'** est une médiathèque de la [ville de Saint-Leu](https://www.saintleu.re/).

L'étage R+3 est équipé de tables et peut vous accueillir pour travailler de façon nomade.

## Restauration
Les nombreux commerces du centre ville vous offriront un large choix pour le midi.