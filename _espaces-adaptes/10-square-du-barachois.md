---
title: Square du Barachois
permalink: /espace-adapte/saint-denis/square-du-barachois/
website: https://www.saintdenis.re/square-du-barachois
coarse_location: Centre ville, Saint-Denis
region: Nord
location:
  latitude: -20.8751 
  longitude: 55.4467
address: Square du Barachois, 97400 Saint-Denis
logo: assets/espaces-adaptes/logo-saint-denis.png
photo: assets/espaces-adaptes/square-du-barachois-01.jpg
---
Le **square du Barachois**, anciennement square Labourdonnais, est un grand parc en face du Barachois à Saint-Denis.

De nombreux bancs permettent de s'y asseoir. Il n'y a cependant aucune prise électrique.

La [boulangerie Paul]({% link _espaces-adaptes/10-paul-barachois.md %}), située juste en face, peut être un point de repli pratique en cas de pluie.