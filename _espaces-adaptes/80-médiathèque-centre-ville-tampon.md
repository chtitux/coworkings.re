---
coarse_location: Centre ville, Le Tampon
region: Sud
location:
  title: Médiathèque du centre ville du Tampon
  latitude: -21.2804273
  longitude: 55.5193645
title: Médiathèque du centre ville du Tampon
permalink:  /espace-adapte/le-tampon/médiathèque-centre-ville/
website: https://www.mediatheque-tampon.fr/Default/rlp-tampon.aspx
address: 16 rue Victor le Vigoureux, 97430 Le Tampon
email: contact@mediatheque-tampon.fr
phone: '+262 262 55 02 19'
logo: assets/espaces-adaptes/logo-mediatheques-du-tampon.png
---
La médiathèque du centre ville du Tampon est une médiathèque de la [ville du Tampon](https://www.letampon.fr/).

## Équipements
Des tables et des chaises sont disponibles dans l'espace multimédia.

## Horaires d'ouverture
La médiathèque est ouverte tous les après-midis, de 13:00 à 18:00. **Elle est fermée le matin**.

## Localisation et accessibilité
La médiathèque se trouve à côté du [Théâtre Luc Donat](https://www.theatrelucdonat.re/), dans le centre ville du Tampon.

Le parc Jean de Cambiaire est à proximité immédiate de la médiathèque.

Un parking est situé à proximité, pour les voitures et les vélos.

### Restauration à proximité
Des snacks et des boulangeries se trouvent à proximité, il est aisé de s'y procurer à manger.

