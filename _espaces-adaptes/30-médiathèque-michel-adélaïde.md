---
coarse_location: Centre ville, Saint-Gilles les Bains
region: Ouest
location:
  latitude: -21.052369213552925
  longitude: 55.22569519179853
title: Médiathèque Michel Adélaïde
permalink: /espace-adapte/saint-gilles-les-bains/médiathèque-michel-adélaïde/
logo: assets/espaces-naturels/logo-ville-saint-paul.png
website: https://www.mairie-saintpaul.re/bibliotheques-mediabus/
phone: '+262 262 70 43 24'
address: rue du Saint-Laurent, 97434 Saint-Gilles les Bains
photo: assets/espaces-adaptes/médiathèque-michel-adélaïde-01.jpg
---
La **Médiatèque Michel Adélaïde** est une médiathèque de la [ville de Saint-Paul](https://www.mairie-saintpaul.re/).

Il est possible de travailler en silence à l'étage. Une terrasse est également disponible, avec une unique prise de courant
et quelques tables supplémentaires.

## Restauration
Le choix de manque pas pour se restaurer autour de la médiathèque.

De nombreux restaurants sont installés place du marché forrain, à quelques dizaines de mètre de la médiathèque.

La plage des Roches Noires et son esplanade sont également pourvues en snack et brasseries.

Enfin, de nombreux points de restauration sont également présents dans la rue du Général de Gaulle à quelques centaines de mètres.