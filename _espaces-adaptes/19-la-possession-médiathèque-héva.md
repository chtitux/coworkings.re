---
coarse_location: Centre ville, La Possession
region: Ouest
location:
  latitude: -20.9261
  longitude: 55.3374
title: Médiathèque Héva
permalink: /espace-adapte/la-possession/médiathèque-héva/
website: https://www.lapossession.re/vivre-a-la-possession/sports-culture/la-mediatheque.html
phone: '+262 262 22 40 00'
address: 100 avenue du 20 Décembre 1848, 97420 Le Port
logo: assets/espaces-adaptes/logo-la-possession.png
---
La **Médiathèque Héva** est une médiathèque de la [ville de La Possession](https://www.lapossession.re/).

# Horaires
La médiathèque est ouverte de **12:00 à 18:00** tous les jours de la semaine, sauf le mercredi où l'ouverture se fait à 09:00. La médiathèque est également ouverte le samedi de 09:00 à 17:00.


# Restauration
Une boulangerie se trouve en face de la médiathèque.
