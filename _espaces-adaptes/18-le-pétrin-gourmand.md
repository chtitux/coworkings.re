---
coarse_location: Centre ville, Le Port
region: Ouest
location:
  latitude: -20.93505962480834
  longitude: 55.29200390140632
title: Le Pétrin Gourmand
permalink: /espace-adapte/le-port/le-pétrin-gourmand/
website: https://www.facebook.com/lepetringourmand/
phone: '+262 262 39 18 48'
address: 13 Rue Léon Lepervanche, 97420 Le Port
logo: assets/espaces-adaptes/logo-banette.png
photo: assets/espaces-adaptes/le-petrin-gourmand-01.jpg
---
**Le Pétrin Gourmand** est une boulangerie avec une large terrasse couverte.

Il est possible de travailler, mais il n'y a pas de prises électriques disponibles
pour charger son ordinateur portable.

Un service à table est proposé le midi. Des sandwiches sont également en vente
à la boulangerie.

