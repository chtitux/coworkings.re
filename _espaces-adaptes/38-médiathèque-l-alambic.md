---
coarse_location: Centre ville, Trois-Bassins
region: Ouest
location:
  latitude: -21.10398
  longitude: 55.2969664
title: Médiathèque de l’Alambic
permalink: /espace-adapte/trois-bassins/mediatheque-de-l-alambic/
website: https://trois-bassins-pom.c3rb.org/
logo: assets/espaces-adaptes/logo-trois-bassins.png
photo: assets/espaces-adaptes/médiathèque-l-alambic.jpg
address: 28 rue Georges Brassens, 97426 Trois-Bassins
phone: '+262 262 74 56 91'
---
La **Médiathèque de l’Alambic** est une médiathèque de la ville de [Trois-Bassins](https://trois-bassins.re/).

Plusieurs tables permettent de travailler à l'intérieur de la médiathèque dans le calme. Il est également possible d'accéder à des prises électriques à certaines places.

Une salle de travail est également présente. Elle est accessible sur demande au personnel de la médiathèque, lorsqu'elle n'est pas utilisée.

Un jardin intérieur permet également de travailler à l'air libre.