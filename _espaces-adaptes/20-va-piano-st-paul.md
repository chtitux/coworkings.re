---
coarse_location: Centre ville, Saint-Paul
region: Ouest
location:
  title: Va Piano
  latitude: -21.00851402658749
  longitude: 55.26964099707016
title: Va Piano Saint-Paul
website: https://www.vapiano.re/
permalink:  /espace-adapte/saint-paul/va-piano/
logo: assets/espaces-adaptes/logo-vapiano.png
address: 6 quai Gilbert, 97460 Saint Paul
phone: '+262 262 43 53 35'
---
Le restaurant **Va Piano** de Saint-Paul est ouvert dès le matin.

Une offre petit-déjeuner vous permet de vous y installer pour travailler.

Quelques prises sont disponibles.