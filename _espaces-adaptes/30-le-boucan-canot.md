---
coarse_location: Boucan, Saint-Gilles les Bains
region: Ouest
location:
  title: Le Boucan Canot
  latitude: -21.02732008604644
  longitude: 55.22654831448565
title: Le Boucan Canot
website: https://www.boucancanot.com/
permalink:  /espace-adapte/saint-gilles-les-bains/hotel-le-boucan-canot/
address: 32 rue du Boucan Canot , 97434 Saint-Gilles les Bains
phone: '+262 262 33 44 44'
email: commercial@boucancanot.com
logo: assets/espaces-adaptes/logo-boucan-canot.png
---
Le Boucan Canot est un hôtel situé à côté de la plage de Boucan.

Il est possible de s'installer à une table pour y travailler de façon nomade.
