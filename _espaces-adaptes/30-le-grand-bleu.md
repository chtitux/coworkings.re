---
coarse_location: Roches Noires, Saint-Gilles les Bains
region: Ouest
location:
  title: Le Grand Bleu
  latitude: -21.053175323017005
  longitude: 55.22472997213629
title: Le Grand Bleu
permalink:  /espace-adapte/saint-gilles-les-bains/hotel-le-grand-bleu/
logo: assets/espaces-adaptes/logo-le-grand-bleu.png
phone: '+262 2 62 02 60 60'
email: contact@hotel-legrandbleu.com
address: 46 Boulevard Roland Garros, 97434 Saint-Gilles les Bains
website: https://www.hotel-legrandbleu.com/
photo: assets/espaces-adaptes/le-grand-bleu-01.jpg
---
Le Grand Bleu est un hôtel restaurant lounge situé à côté de la plage des Roches Noires, à Saint-Gilles les Bains.

# Restauration
Un restaurant est disponible sur place. La formule plat du jour avec entrée, plat et dessert à 26 € est disponible avec le forfait coworking à 14 €.

# Services
Il n'y a pas de places de parking disponible.
L'espace de coworking est situé dans le halll d'entrée de l'hôtel, à proximité du bar et de la piscine.

# Tarifs
Une formule coworking est disponible à partir de 14 €. Elle comprend 2 boissons chaude et une bouteille d'eau. Il est également possible d'accéder à la piscine jusqu'à 17:00.


