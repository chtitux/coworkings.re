---
coarse_location: La Saline les Bains
region: Ouest
location:
  title: Le Ness by D-Ocean
  latitude: -21.0954938239013
  longitude: 55.23754005853744
title: Le Ness by D-Ocean
website: https://www.nessbydocean.com/fr
permalink:  /espace-adapte/la-saline-les-bains/le-ness-by-d-ocean/
logo: assets/espaces-adaptes/logo-le-ness-by-d-ocean.png
photo: assets/espaces-adaptes/le-ness-by-d-ocean-01.jpg
address: 26 route de Trou d'Eau, 97434 Saint Gilles les Bains
phone: '+262 262 70 30 00'
email: contact@nessbydocean.com
---
Le Ness *by* D-Ocean est un hôtel 4 étoiles.

La terrasse avec accès direct à la plage pourra vous accueillir pour du travail ponctuel.

Les tables hautes sont réservées aux fumeurs, il n'est pas toujours confortable de travailler dans les fauteuils bas avec votre ordinateur sur les genous.

## Équipement
Seules quelques prises électriques sont disponibles sur la terrasse.
