---
coarse_location: Chaudron, Sainte-Clotilde
region: Nord
location:
  latitude: -20.89785
  longitude: 55.4972861
title: Le Village
permalink: /espace-adapte/sainte-clotilde/le-village/
website: https://www.facebook.com/levillagechaudron/
address: 69 boulevard du Chaudron, Sainte-Clotilde
logo: assets/espaces-adaptes/logo-le-village.png
photo: assets/espaces-adaptes/le-village-entrée.jpg
---
**Le Village** est une gallerie commerciale située dans le quartier du Chaudron à Saint-Denis.

# Horaires
L'espace est ouvert de 06:30 à 22:30

# Équipements

Il y a 270 places assises à l'étage, où se situent plusieurs restaurants. Il est possible de s'y installer pour travailler et quelques prises sont disponibles sur des tables hautes.

# Restauration
{% picture assets/espaces-adaptes/le-village-étage.jpg width="250px" class="photo-float-right" alt="Entrée du Village" %}

L'espace est organisé autour de 8 restaurents de spécialités différentes : sushis, pizzas, crêpes, salades, burgers, carry, etc.

# Parking
Un grand parking entoure la gallerie commerciale.