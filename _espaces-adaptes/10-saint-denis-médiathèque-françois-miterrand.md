---
coarse_location: Centre ville, Saint-Denis
region: Nord
location:
  latitude: -20.8942973
  longitude: 55.468709
title: Médiathèque François Miterrand
permalink: /espace-adapte/saint-denis/médiathèque-françois-miterrand/
website: https://lecturepublique.cinor.org/iguana/www.main.cls?surl=saint-denis
email: secretariat@mediatheque.saintdenis.re
phone: '+262 262 94 28 88'
address: 1 rue de l'Europe 97400 Saint-Denis
logo: assets/espaces-adaptes/logo-cinor.png
---
La **Médiathèque François Miterrand** est une médiathèque située dans la [ville de Saint-Denis](https://www.saintdenis.re/) et gérée par la [Cinor](https://www.cinor.re/), communauté de communes du Nord de la Réunion.

# Horaires
La médiathèque est ouverte du mardi au vendredi de 09:00 à 18:00 tous les jours, sauf le jeudi où l'ouverture se fait à 13:00. 

# Équipements
Une grande salle permet de travailler en silence sur des tables individuelles.

Dans les différents espaces, une centaine de places assises sont également présentes, pour un travail seul ou en petit groupe.

Deux espaces de co-working sont également réservables gratuitement pour le travail en groupe dans des espaces privatisés. Il est nécessaire de se renseigner à l'accueil de la médiathèque à l'avance.