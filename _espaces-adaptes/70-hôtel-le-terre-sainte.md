---
coarse_location: Terre Sainte, Saint-Pierre
region: Sud
location:
  title: Hôtel le Terre Sainte
  latitude: -21.34349480879006
  longitude: 55.47983858021756
title: Hôtel le Terre Sainte
permalink:  /espace-adapte/saint-pierre/hotel-le-terre-sainte/
website: https://www.leterresainte.re/
address: 13 rue de l’Amiral Lacaze, 97410 Saint-Pierre
email: reservation@leterresainte.re
phone: '+262 262 81 74 45'
logo: assets/espaces-adaptes/logo-le-terre-sainte.png
photo: assets/espaces-adaptes/le-terre-sainte-interieur.jpg
---
L'hôtel **Le Terre Sainte** est un hôtel situé à Saint-Pierre qui propose un espace de coworking. 

Il est situé dans le quartier de Terre Sainte à Saint-Pierre.

## Équipements
L'espace coworking est ouvert sur l'extérieur : il permet de s'installer à l'ombre ou dehors. Il n'y a pas de climatisation, des ventilateurs et la brise marine aèrent le lieu.

Un réseau Wifi non sécurisé et gratuit est accessible.

Plusieurs tables, dont des certaines avec des sièges hauts, sont équipées de prises électriques.

Il n'y a pas de cabine pour s'isoler, mais l'espace est suffisamment grand pour trouver une place au calme.

## Services
### Salle de réunion
Une salle de séminaire est présente et peut être louée.

### Snack
Un service de snack est disponible toute la journée.

## Prix et formules
Deux formules sont proposées :
- Une formule à 7 € TTC : 1 demi-journée et un café
- Une formule à 9 € TTC : 1 demi-journée, 1 café et des viennoiseries

## Localisation et accessibilité
{% picture assets/espaces-adaptes/le-terre-sainte.jpg width="250px" class="photo-float-right" alt="Façade du Terre Sainte" %}

Le Terre Sainte est situé à l'entrée du quartier Terre Sainte.
Il est accessible à pied depuis le front de mer de Saint-Pierre.

Le parking gratuit de la ravine de la Rivière d'Abord est à 300 mètres. Il est formellement déconseillé de s'y garer en cas de pluie, le parking pouvant être inondé rapidement.

### Restauration à proximité
Il est très facile de se restaurer à proximité de l'hôtel. De nombreux restaurants et snacks sont situés à Terre Sainte et sur le front de mer.
