---
coarse_location: Centre ville, Le Port
region: Ouest
location:
  latitude: -20.93851
  longitude: 55.29799
title: Médiathèque Benoîte Boulard
permalink: /espace-adapte/le-port/médiathèque-benoîte-boulard/
website: https://mediatheque.ville-port.re/
email: mediatheque@ville-port.re
phone: '+262 262 43 50 91'
address: 100 avenue du 20 Décembre 1848, 97420 Le Port
logo: assets/espaces-adaptes/logo-le-port.png
photo: assets/espaces-adaptes/mediatheque-benoite-boulard.jpg

---
La **Médiathèque Benoîte Boulard** est une médiathèque de la [ville du Port](https://www.ville-port.re/).

# Horaires
La médiathèque est ouverte de 10:00 à 18:00 tous les jours, sauf le jeudi où l'ouverture se fait à 12:00. 

# Équipements
De nombreuses salles sont ouvertes au public. Une salle de travail est également disponible avec des tables.

# Restauration
Le [snack Gootsa](https://www.gootsa.re/) est présent au rez-de-chaussée de la médiathèque.

De nombreux autres snacks se trouvent également à proximité.

# Transports
La médiathèque est située en face de la gare routière du Port. Il est possible de prendre les bus [Car Jaune](https://www.carjaune.re/) et [Kar'Ouest](https://www.karouest.re/).

