---
title: Jardin de l'État
permalink: /espace-adapte/saint-denis/jardin-de-l-etat/
website: https://www.reunion.fr/offres/jardin-de-l-etat-le-saint-denis-fr-558236/

coarse_location: Centre ville, Saint-Denis
region: Nord
location:
  title: Jardin de l'État
  latitude: -20.887020448406282
  longitude: 55.45085107556626
address: Place de Metz, 97400 Saint-Denis
---
Le **Jardin de l'État** est un grand parc au cœur de Saint-Denis.

Il est possible de s'installer dans le restaurant [l'Oiseau du Jardin](https://www.facebook.com/oiseaudujardin/),
mais prévoyez d'avoir une batterie pleine, il n'y a pas de prise électrique disponible.

Le cadre verdoyant vous permettra d'y passer un bon moment.

Attention aux moustiques, forts présents dans le jardin. Amenez votre bombe anti-moustique.
