---
coarse_location: Barachois, Saint-Denis
region: Nord
address: 3 avenue de la Victoire, 97400 Saint-Denis
phone: '+262 262 56 05 60'
location:
  latitude: -20.8741
  longitude: 55.4469
title: Paul
permalink: /espace-adapte/saint-denis/paul-barachois/
website: https://www.facebook.com/paulreunion974/
logo: assets/espaces-adaptes/logo-paul-reunion.png
---
**Paul** est une boulangerie située en face du [square du Barachois]({% link _espaces-adaptes/10-square-du-barachois.md %}).

Elle propose de la restauration rapide (sandwichs, salades, etc.). De nombreuses tables sont présentes pour vous y asseoir et y travailler.

La boulangerie est ouverte tous les jours, de 06:00 à 19:00.