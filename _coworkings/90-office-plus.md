---
coarse_location: Saint-André
region: Est
location:
  latitude: -20.963752810013844
  longitude: 55.65939940721141
title: Office Plus
permalink: /coworking/saint-andre/office-plus/
website: https://yellow.place/fr/office-plus-saint-andr%C3%A9-reunion
phone: '+262 692 22 33 47'
address: Local 2 ESPACE TARANI, 95 Pente Sassy, 97440 Saint-André
---
**Office Plus** est un centre d'affaires permettant de louer des bureaux et des salles
de réunion.

## Services
Il est possible de faire domicilier son courrier sur place.

Un service de secrétariat et permanence téléphonique est également disponible.