---
coarse_location: Saint-Louis
region: Sud
location:
  latitude: -21.291717
  longitude: 55.405211
title: The Greenhouse Coworking
permalink: /coworking/saint-louis/the-greenhouse-coworking/
website: https://www.tghcoworking.com/
logo: assets/coworkings/logo-thegreenhouse.png
photo: assets/coworkings/the-greenhouse-coworking-interieur.jpg
address: 4 Avenue du Père René Payet, 97450 Saint-Louis
email: contact@tghcoworking.com
phone: '+262 692 57 80 20'
---
**The Greenhouse** est un espace de coworking à Saint-Louis.

Il est possible d'y travailler en open space, ainsi que de louer des salles de réunion.

Des bureaux privatifs sont également proposés.

## Équipements
{% picture assets/coworkings/the-greenhouse-coworking-fenetres.jpg width="250px" class="photo-float-right" alt="Open-space avec vue extérieure" %}

L'accès à Internet avec la fibre, à l'espace détente ainsi qu'une cuisine partagée équipée est compris dans l'offre.

Une douche est également à disposition.

## Restauration
Un snack à côté du lycée fait de la restauration rapide.

## Autour du site
La gare de bus de Saint-Louis est située à proximité. Elle permet d'effectuer des voyages en [Car Jaune](https://www.carjaune.re/) ou [Alternéo](https://www.alterneo.re/).

Le centre-ville de Saint-Louis est également proche. De nombreux commerces et boutiques de proximité s'y trouvent.
