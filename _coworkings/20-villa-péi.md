---
coarse_location: Cambaie, Saint-Paul
region: Ouest
location:
  title: Villa Péi
  latitude: -20.96221302090073
  longitude: 55.30181601578524
title: Villa Péi
permalink: /coworking/saint-paul/villa-péi/
website: https://www.facebook.com/lavillapei
logo: assets/coworkings/logo-villa-pei.png
photo: assets/coworkings/villa-pei-01.jpg
---
La **Villa Péi** est un espace de coworking et de location de salles de réunion à Cambaie, dans la ville de Saint-Paul.

La grande salle dispose d'une dizaine de postes en open space.

De grands jardins permettent de disposer d'un cadre agréable pour travailler.

## Équipements
Une piscine est à disposition.

Il est possible de commander un repas préparé par un chef, idéalement la veille. Une assiette ainsi qu'un dessert et un soda sont servis sur place pour 15 € HT.
