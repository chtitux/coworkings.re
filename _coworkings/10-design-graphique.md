---
title: Design Graphique
permalink: /coworking/saint-denis/design-graphique/
coarse_location: Centre ville, Saint-Denis
region: Nord
website: https://wa.link/zhxc9d
address: 1 Rue Charles Gounod, 97400 Saint Denis
phone: '+262 692 70 66 67'
email: annabelgoetz.designgraphique@gmail.com
photo: assets/coworkings/design-graphique-saint-denis.jpg
location:
  latitude: -20.87469
  longitude: 55.4531802
---
**Design Graphique** propose la location d'espace de coworking à Saint-Denis.

Deux espaces de travail et une salle de réunion sont disponibles à la location.

## Tarifs
* 20 € l'heure
* Forfait 10 heures et 40 heures sur demande, valables 2 mois

