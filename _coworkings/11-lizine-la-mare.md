---
title: LIZINE La Mare
permalink: /coworking/sainte-marie/lizine-la-mare/
website: https://www.lizine.com/espaces/la_mare/
coarse_location: La Mare, Sainte-Marie
region: Nord
address: Cuves de La Mare 2 - Bâtiment C, 30 rue André Lardy, 97438 Sainte-Marie
location:
  latitude: -20.894886
  longitude: 55.533388
photo: assets/coworkings/lizine-la-mare.jpg
logo: assets/coworkings/logo-lizine.svg
---
[LIZINE La Mare](https://www.lizine.com/espaces/la_mare/) est un coworking du réseau [LIZINE](https://www.lizine.com/), géré par [CBo Territoria](https://www.cboterritoria.com/).

Il est situé dans le quartier de La Mare à Sainte-Marie, sur le site de [l'ancienne usine de cannes à sucre](https://randopitons.re/tourisme/544-usine-mare). Il est à quelques centaines de mètres du [Hub Lizine]({% link _coworkings/11-lizine-le-hub.md %}).

L'espace de coworking est situé à l'étage. Trois cabines permettent de s'isoler pour passer des appels sans déranger ses voisins.

Une table haute dehors vous permettra de prendre une pause ou de travailler à l'extérieur.

## Offres
Plusieurs offres sont proposées, comme sur les autres espaces du groupe LIZINE :
* Location à la demi-journée ou à la journée
* Location au mois, avec possibilité d'un accès 24/7

Par ailleurs, des bureaux privatifs sont également proposés.

> info "Une offre multi-site"
> LIZINE propose des offres permettant l'accès à ses autres espaces de coworking de [Savanna à Saint-Paul]({% link _coworkings/20-lizine-savanna.md %}) et [Grand-Bois à Saint-Pierre]({% link _coworkings/70-lizine-grand-bois.md %}).

## Services
Plusieurs services utiles aux entrepreneurs sont disponibles :
* Service d'impression de document
* Location de salles de réunion
* Domiciliation de votre courrier
