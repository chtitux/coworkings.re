---
title: LIZINE Grand Bois
permalink: /coworking/saint-pierre/lizine-grand-bois/
website: https://www.lizine.com/les-espace-grand-bois/
coarse_location: Grand Bois, Saint-Pierre
region: Sud
address: 249 avenue du Général de Gaulle, 97410 Saint-Pierre
location:
  title: LIZINE Grand Bois
  latitude: -21.35991660074975
  longitude: 55.526068333326094
logo: assets/coworkings/logo-lizine.svg
---
[LIZINE Grand Bois](https://www.lizine.com/les-espace-grand-bois/) est un espace de coworking du réseau [LIZINE](https://www.lizine.com/), géré par [CBo Territoria](https://www.cboterritoria.com/).

Les espaces de travail nomades sont situés à l'intérieur d'une grande salle où se trouvent de nombreux bureaux privatifs.

## Offres
Plusieurs offres sont proposées, comme dans les autres espaces du réseau LIZINE :
* Location à la demi-journée ou à la journée
* Location au mois, avec possibilité d'un accès 24/7

Par ailleurs, des bureaux privatifs sont également proposés.

> info "Une offre multi-site"
> LIZINE propose des offres permettant l'accès à ses autres espaces de coworking de [La Mare à Sainte-Marie]({% link _coworkings/11-lizine-la-mare.md %}) et [Savanna à Saint-Paul]({% link _coworkings/20-lizine-savanna.md %}).

## Restauration
Une boulangerie ainsi qu'un snack à l'intérieur du parking vous donneront toutes les options pour vous restaurer le midi.

## Autour du site
La [plage de Grand-Bois](https://guide-reunion.fr/plage-de-grand-bois/) est située en face du bureau. Le cadre est maginique et il est formellement interdit de s'y baigner pour des raisons de sécurité.

La [plage de Grand-Anse](https://www.sudreuniontourisme.fr/tresors-du-sud/la-plage-de-grande-anse.html) est à quelques minutes en voiture. C'est un *spot* parfait pour admirer le coucher de soleil après une journée de travail intense.
