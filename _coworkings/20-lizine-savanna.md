---
title: LIZINE Savanna
permalink: /coworking/saint-paul/lizine-savanna/
website: https://www.lizine.com/espaces/savanna/
coarse_location: Savanna, Saint-Paul
region: Ouest
address: Bât A, 14 rue Jules Thirel, 97460 Saint-Paul
location:
  title: LIZINE Savanna
  latitude: -20.987726
  longitude: 55.297820
photo: assets/coworkings/lizine-savanna.jpg
logo: assets/coworkings/logo-lizine.svg
email: savanna@lizine.com
---
[LIZINE Savanna](https://www.lizine.com/espaces/savanna/) est un coworking du réseau [LIZINE](https://www.lizine.com/), géré par [CBo Territoria](https://www.cboterritoria.com/). Il est située dans le quartier Savanna de Saint-Paul, à proximité de la voie rapide.

## Équipements
L'open-space est équipé de chaises et bureaux avec prises individuelles. Un réseau Wifi est également proposé.

12 places sont disponibles dans l'open-space.

Il est également possible de s'installer sur la terrasse protégée par un voile d'ombrage. La terrasse n'est cependant pas équipée de prise électrique.

## Services
### Bureaux privatifs
Plusieurs bureaux privatifs sont disponibles sur le site.

### Salle de réunion
Des salles de réunion de différentes tailles sont proposé à la location pour la journée ou la demi-journée.

### Snacking
Une machine à café et un distributeur de snacks

### Domiciliation
Un service de domiciliation est proposé. Le courrier peut être retiré sur place.

## Prix et formules
- la journée en open-space est à 20€ HT, la demi-journée à 10 €
- L'abonnement résident permet d'avoir accès à l'espace 24h/24 et 7 jours sur 7 pour 239 € HT

## Localisation et accessibilité
- Proximité de site touristiques
- Parking?

### Restauration à proximité

L'espace de coworking est spacieux et propose plusieurs postes en openspace,
une terrasse accessible, un coin cuisine avec un micro-onde ainsi qu'une douche.

## Offres
Plusieurs offres sont proposées :
* Location à la demi-journée ou à la journée
* Location au mois, avec possibilité d'un accès 24/7

Par ailleurs, des bureaux privatifs sont également proposés.

> info "Une offre multi-site"
> LIZINE propose des offres permettant l'accès à ses autres espaces de coworking de [La Mare à Sainte-Marie]({% link _coworkings/11-lizine-la-mare.md %}) et [Grand-Bois à Saint-Pierre]({% link _coworkings/70-lizine-grand-bois.md %}).
