---
coarse_location: Saint-Benoît
region: Est
address: Atelier 3 Zone d'activité Pôle Bois, 86 rue Cryptomérias, 97470 Saint-Benoit
location:
  latitude: -21.05179269812947
  longitude: 55.71803279502783
title: Reusit Saint Benoît
permalink:  /coworking/saint-benoit/reusit/
website: https://www.reusit.re/
phone: '+262 262 97 11 52'
logo: assets/coworkings/logo-reusit.png
---
Association Réunion Situation est une couveuse d'entreprise.

Elle permet de louer des espaces de travail.

> info "Un autre espace à Saint-Pierre"
> [Réu.sit de Saint-Pierre]({% link _coworkings/70-reusit-saint-pierre.md %}) est un lieu du même groupe

