---
coarse_location: Grand Fond, Saint-Gilles les Bains
region: Ouest
email: atmos.sphere123@gmail.com
location:
  title: Atmosphère 123
  latitude: -21.037319993611533
  longitude: 55.217725614848355
title: Atmosphère 123
permalink: /coworking/saint-gilles-les-bains/atmosphere-123/
website: https://atmosphere123.business.site/
photo: assets/coworkings/atmosphere-123-01.jpg
---
**Atmosphère 123** est un espace de coworking situé à Grand Fond, juste en face de la plage de Cap Homard.

Une grande salle avec 4 postes de coworking permet de travailler dans le calme.
Un bar et des tables à l'extérieur permettent de travailler avec vue sur l'Océan Indien.

## Équipement
Une grande cuisine est disponible, avec micro-onde et frigidaire.

Des places de parking sont disponibles dans l'enceinte du coworking.
