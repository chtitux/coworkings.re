---
title: My Office Club
permalink: /coworking/le-port/my-office-club/
website: https://www.facebook.com/profile.php?id=100076126153365
phone: '+262 692 66 67 56'
email: direction@dps-reunion.com
address: 9 rue Jeanne d’Arc, Le Port
coarse_location: Centre ville, Le Port
region: Ouest
location:
  latitude: -20.934141310891853
  longitude: 55.29308397664889
logo: assets/coworkings/logo-my-office-club.png
photo: assets/coworkings/my-office-club-01.jpg
---
**My Office Club** est un espace de coworking situé dans la ville du Port, dans le centre ville.

## Offres
Une offre d'abonnement permet d'avoir des réductions sur le tarif à l'heure ou la journée.

Des salles de réunions sont également disponibles à la location.

## Restauration
Une machine à café est disponible sur place, dans une cuisine très bien aménagée. Plusieurs restaurants et une boulangerie sont également à proximité, sur la place des Cheminots.
