---
coarse_location: Parc Technor, Sainte-Clotilde
region: Nord
address: 20, rue Maxime Rivière, Parc Technor, 97490 Sainte-Clotilde
location:
  title: Le Village by CA
  latitude: -20.905280919645875
  longitude: 55.50092055394529
title: Le Village by CA
permalink: /coworking/sainte-clotilde/le-village-by-ca/
website: https://reunion.levillagebyca.com/
logo: assets/coworkings/logo-le-village-by-ca.svg
photo: assets/coworkings/le-village-by-ca-facade.jpg
email: reunion@levillagebyca.com
---
**Le Village by CA** est un lieu du Crédit Agricole.

Il est possible d'y travailler en tant que nomade si des bureaux sont disponibles.

## Services
Le Village by CA est incubateur d'entreprises.

## Restauration
Un service de restauration est présent sur place.
De nombreuses tables dehors permettent de manger en toute tranquilité.

