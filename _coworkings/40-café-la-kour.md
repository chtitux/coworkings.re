---
coarse_location: Les Quatre Robinet, Saint-Leu
region: Ouest
location:
  latitude: -21.19545119032137
  longitude: 55.29238049540175
title: Café la Kour
permalink: /coworking/saint-leu/cafe-la-kour/
website: https://www.facebook.com/cafelakour
logo: assets/coworkings/logo-café-la-kour.png
photo: assets/coworkings/la-kour-interieur.jpg
address: 51 rue du Musée, Saint-Leu
phone: '+262 693 92 79 23'
email: tierslieulakour@gmail.com
---
Le **Café La Kour** est un tiers-lieu qui permet de s'installer à des travailleurs nomades.

## Équipements
Le lieu est équipé de plusieurs salles et salons extérieurs où il est possible de s'installer pour y travailler.
Un réseau Wifi est disponible.

## Évènements
Des concerts et des rencontres sont organisés régulièrement. La [page Facebook de l'association](https://www.facebook.com/cafelakour) annonce les prochains évènements.

## Bar
Un bar est situé dans l'enceinte de La Kour. Une micro-brasserie est également installée.

{% picture assets/coworkings/la-kour-terrasse.jpg width="250px" class="photo-float-right" alt="Terrasse du bar de La Kour" %}
