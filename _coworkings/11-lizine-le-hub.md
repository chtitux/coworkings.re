---
title: LIZINE Le Hub
permalink: /coworking/sainte-marie/lizine-le-hub/
website: https://www.lizine.com/hubinnovation/notre-mission/
coarse_location: La Mare, Sainte-Marie
region: Nord
address: 8 rue Pondichéry, ZI La Mare, 97438 Sainte Marie
location:
  title: LIZINE Le Hub
  latitude: -20.894097
  longitude: 55.531582
logo: assets/coworkings/logo-lizine.svg
---
[LIZINE Le Hub](https://www.lizine.com/hubinnovation/notre-mission/) est un lieu conçu par [CBo Territoria](https://www.cboterritoria.com/). Plusieurs projets et entreprises y sont incubés, et il est également possible d'y venir travailler. Il fait partie du réseau [LIZINE](https://www.lizine.com/).

**Le Hub** se trouve à proximité de [LIZINE La Mare]({% link _coworkings/11-lizine-la-mare.md %}), dans le quartier de La Mare à Sainte-Marie. Il est possible de travailler en extérieur. La situation en bout de piste de [l'aéroport Roland Garros](https://www.reunion.aeroport.fr/) permet d'avoir une vue dégagée, à condition d'accepter le bruit des quelques décollages dans la journée.

## Offres
Sous réserve de disponibilité, il est possible de travailler en tant que nomade sur le site.

Le Hub propose également une grande salle de réunion modulable, capable d'accueillir plusieurs dizaines de personnes en fonction de sa configuration.

## Restauration

> note "Un service restauration sur place"
> Il est possible d'acheter un repas le midi, ainsi que des boissons froides ou chaude sur place.
