---
coarse_location: La Mare, Sainte-Marie
region: Nord
address: 9 rue Pierre Marinier 97438 Sainte Marie
phone: '+262 262 830 830'
email: hello@domicializ.fr
location:
  latitude: -20.8971325771431
  longitude: 55.530951353463585
logo: assets/coworkings/logo-domicializ.svg
photo: assets/coworkings/domicializ-facade.jpg
title: Domicializ
permalink: /coworking/sainte-marie/domicializ/
website: https://www.domicializ.fr/
---
**Domicializ** propose des espaces de travails aux coworkers. Il est possible de travailler en open-space ou dans des bureaux privatifs.

## Services
Il est possible de faire domicilier son courrier chez Domicializ.

Des salles de réunion et des bureaux privatifs sont également disponibles.

Enfin, le lieu est associé à [Boxea](https://www.boxea.fr/), un service de garde meubles.

{% picture assets/coworkings/domicializ-openspace.jpg width="250px" class="photo-float-right" alt="Open-space de Domicializ" %}
