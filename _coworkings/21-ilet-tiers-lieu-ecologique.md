---
coarse_location: Plateau Caillou, Saint-Paul
region: Ouest
location:
  latitude: -21.023262252418515
  longitude: 55.27304344110803
title: L'Îlet tiers lieu écologique 
permalink: /coworking/saint-paul/îlet-tiers-lieu-écologique/
website: https://www.facebook.com/profile.php?id=61560903477814
phone: '+262 693 84 64 16'
email: lea.lafeeverte@gmail.com
address: 15 rue des Merles, Plateau Caillou, 97460 Saint-Paul
photo: assets/coworkings/ilet-tiers-lieu-ecologique-01.jpg
---
L'Îlet tiers lieu écologique est un petit coworking à taille humaine au sein d'un tiers lieu regroupant des bureaux, un atelier et un jardin pédagogique.

Il est située à Plateau Caillou Saint-Paul, avec une forte implication écologique.
