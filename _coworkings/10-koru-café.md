---
title: Koru Café
permalink: /coworking/saint-denis/koru-café/
coarse_location: Centre ville, Saint-Denis
website: https://korucafe.fr/
logo: assets/coworkings/logo-koru-cafe.png
photo: assets/coworkings/koru-cafe-openspace.jpg
region: Nord
address: 57 rue Pasteur, 97400 Saint Denis
phone: '+262 693 48 58 03'
email: contact@korucafe.fr
location:
  latitude: -20.878248370812816
  longitude: 55.452537045192024
---
Le **Koru Café** est un café avec un espace de coworking dans le centre-ville de Saint-Denis.

## Équipements
L'open-space et les autres salles de travail sont toutes équipées de tables et de chaises. Des prises électriques individuelles sont également disponibles dans l'open-space.

Un réseau Wifi est proposé.

Il n'y a pas de lieu pour s'isoler, mais la petite salle de réunion Queenstown est parfois disponible pour s'isoler.

## Services
### Salle de réunion
Plusieurs salles de réunion sont disponibles :
- salle Auckland, capacité de 15 personnes
- salle Queenstand, capacité de 4 personnes
- salle Coromandel, capacité de 6 personnes
- salle Mahia, capacité de 4 personnes

### Snack et restauration sur place
{% picture assets/coworkings/koru-cafe-rdc.jpg width="250px" class="photo-float-right" alt="Rez-de-chaussée du Koru café" %}

Le **Koru Café** étant également un lieu de restauration, il fournit gratuitement aux clients des espaces de travail du café et des petites friandises.

Il est également possible de consommer les produits vendus au rez-de-chaussée\ : quiche, salade, *bowl*, sandwich, etc.

## Prix et formules
Les [tarifs](https://korucafe.fr/tarifs/) comprennent des locations à la journée ou par abonnement.

L'open-space est accessible aux tarifs suivants :
- 6 € par heure
- 20 € la demi-journée
- 28 € la journée

## Localisation et accessibilité
Le lieu est situé en plein centre-ville de Saint-Denis, rue Pasteur. De nombreux [parkings payants](https://citypark.re/parkings/) et de places de stationnement sont situés à proximité.

Le carré Cathédrale, où se trouvent de nombreux restaurants et bars est à proximité immédiate.

### Restauration à proximité
De nombreux restaurants et snacks sont présents dans le centre-ville de Saint-Denis, le choix est large.

## Communauté Bann'Coworkers
{% picture assets/communautes/logo-bann-coworkers.jpg width="150px" class="photo-float-right" alt="Logo Bann'Coworkers" %}

Plusieurs membres du groupe [Bann' Coworkers](https://banncoworkers.re/) y sont généralement installés.
N'hésitez pas à poser la question aux coworkers à côté de vous, il y en a sûrement un ou une du groupe.