---
coarse_location: La Saline les Bains
region: Ouest
location:
  latitude: -21.09481304
  longitude: 55.23900519
title: Le Crew
permalink: /coworking/la-saline-les-bains/le-crew/
website: https://www.lecrew.re/
address: Salle n°6, 8 rue des Argonautes, La Saline
phone: '+33 6 80 40 41 76'
email: mathilde@lecrew.re
logo: assets/coworkings/logo-le-crew.png
photo: assets/coworkings/le-crew-02.jpg
---
**Le Crew** est un espace de coworking et une communauté d'entrepreneurs et de télé-travailleurs.

Il est situé au [centre de séminaire Tamarun](https://www.tamarun.fr/) à la Saline les Bains.

Il occupe la **salle n°6** du centre de séminaire.

## Communauté
L'adhésion à la communauté du Crew est possible.
Elle permet d'accéder à 1 jour par mois de coworking et des évènements organisés au Crew.
