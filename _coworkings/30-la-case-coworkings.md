---
coarse_location: Grand Fond, Saint-Gilles les Bains
region: Ouest
email: reservation@lacasecoworking.re
location:
  latitude: -21.03888
  longitude: 55.219526
title: La case coworking
permalink: /coworking/saint-gilles-les-bains/la-case-coworking/
website: https://lacasecoworking.re/
logo: assets/coworkings/logo-la-case-coworking.png
photo: assets/coworkings/la-case-coworking-01.jpg
email: contact@lacasecoworking.re
phone: '+262 262 01 24 58'
address: '12 rue de la Cheminée, 97434 Saint Paul'
---
**La case coworking** est un espace de coworking situé dans le quartie de Grand Fond, à Saint-Gilles.

Il est idéalement situé en face d'un grand parking, et au milieu d'une zone de commerces : boulangeries, restaurants, épicerie, magasin d'électronique, etc.

6 places en open-space sont disponibles dans le lieu.

## Équipement
L'espace promeut une politique de réduction de déchet. Pas de capsules de café, mais un café coulé chaque matin, à volonté.

Une salle de réunion spacieuse est également disponible à la location.

## Restauration
La Case est située à l’étage de l’épicerie/restaurant Vrac Bio et Zéro déchet [Wake Up](https://www.facebook.com/wakeup.re/).

Il est possible d'y réserver un repas et de bénéficier de 10% de réduction lors d'une location à La Case.
