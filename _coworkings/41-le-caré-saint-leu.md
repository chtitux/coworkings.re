---
coarse_location: Piton Saint-Leu
region: Ouest
location:
  latitude: -21.218858
  longitude: 55.311896
title: Le CARé, Piton Saint-Leu
permalink:  /coworking/piton-saint-leu/le-caré/
logo: assets/coworkings/logo-le-caré.png
email: contact@lecare.re

website: https://lecare.re/
---
Le **CARé Saint-Leu** est un espace de coworking à Piton Saint-Leu.

> info "Un autre espace à Saint-Denis"
> [Le CARé de Saint-Denis]({% link _coworkings/10-le-caré-saint-denis.md %}) est un lieu du même groupe

## Services
Il est possible de faire domicilier son entreprise sur place.

Des salles de réunion et des bureaux privatifs sont également disponibles.


