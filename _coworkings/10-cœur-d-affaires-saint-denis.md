---
title: Cœur d'affaires, Saint-Denis
permalink: /coworking/saint-denis/cœur-d-affaires/
coarse_location: Centre ville, Saint-Denis
website: https://www.coeurdaffaires.fr/
logo: assets/coworkings/logo-cœur-d-affaires.png
address: 14 rue Pasteur, 97400 Saint-Denis
region: Nord
location:
  latitude: -20.879268706060365
  longitude: 55.44961611235861
---
Le Cœur d'affaires à Saint-Denis est l'établissement du réseau du centre d'affaires Cœur d'affaires du chef-lieu.

> info "Un autre espace à Saint-Paul"
> [Le Cœur d'affaires de Saint-Paul]({% link _coworkings/20-cœur-d-affaires-saint-paul.md %}) est un lieu du même groupe

## Équipements
La salle **Charlie** est l'espace dédié aux coworkers. Elle est équipée de chaises et de tables avec prises électriques. Un réseau wifi est également disponible.

Il est possible d'accéder au *rooftop* et à la cuisine aménagée.

## Services
Le réseau Cœur d'affaires est associé au cabinet d'expertise comptable [Valorens](https://www.valorens.re/). Ce dernier propose une large gamme de services relatifs à la création et à la gestion d'une entreprise : domiciliation, rédaction des status, démarche de création et de gestion d'une entreprise, conseils, etc.

### Salles de réunion
Deux salles de réunion sont présentes.

La salle **Gatsby**, espace de direction, a une capacité de 6 personnes. La salle **Joséphine**, a une capacité de 10 personnes.

Les salles de réunion ne sont pas toujours disponibles, il est important de contacter le Cœur d'affaires en avance pour connaître leur disponibilité.

### Domiciliation
Il est possible de faire domicilier son entreprise sur place. Un site dédié, [sedomicilieralareunion.re/](https://www.sedomicilieralareunion.re/), présente les offres du Cœur d'affaires.

## Localisation et accessibilité
Le Cœur d'Affaires de Saint-Denis est situé rue Pasteur, à proximité de l'hôtel de ville de Saint-Denis.

De nombreuses places de parking sont disponibles dans le centre-ville. Le [parking payant République](https://citypark.re/parkings/republique/) est également situé à proximité.

La place du Barachois et la mer sont à 10 minutes de marche. 

### Restauration à proximité

L'offre en restauration du centre-ville de Saint-Denis est très riche. De nombreux restaurants sont à proximité immédiate du Cœur d'Affaires.

La Place Cathédrale, à 300 mètres, rassemble de nombreux bars et restaurants ouverts en soirée.