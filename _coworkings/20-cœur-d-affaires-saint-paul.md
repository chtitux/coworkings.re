---
coarse_location: Centre ville, Saint-Paul
region: Ouest
address: 20 place du Général de Gaulle, 97460 Saint-Paul
email: accueil@coeurdaffaires.fr
phone: '+262 693 47 40 62'
location:
  latitude: -21.009012650374267
  longitude: 55.27003239315273
title: Cœur d'affaires, Saint-Paul
permalink: /coworking/saint-paul/cœur-d-affaires/
website: https://www.coeurdaffaires.fr/
logo: assets/coworkings/logo-cœur-d-affaires.png
---
**Cœur d'affaires** est un espace de coworking et de bureaux d'entreprise.

Il est situé en plein cœur de la ville de Saint-Paul, à côté de l'hôtel de ville.

Tous les commerces du centre-ville sont accessibles à pied.

# Services
Un service de domiciliation est proposé.

De plus, l'espace est associé au cabinet comptable [Valorens](https://www.valorens.re/).
Il propose des formules de création et de gestion de sociétés.

> info "Un autre espace à Saint-Denis"
> [Le Cœur d'affaires de Saint-Denis]({% link _coworkings/10-cœur-d-affaires-saint-denis.md %}) est un lieu du même groupe
