---
coarse_location: Moufia, Saint-Denis
region: Nord
address: 1 route du Moufia, 97400 Saint-Denis
location:
  latitude: -20.896829345649895
  longitude: 55.4920701709279
phone: '+262 262 284 973'
email: contact@lecare.re
title: le CARé, Saint-Denis
permalink: /coworking/saint-denis/le-caré/
website: https://lecare.re/
logo: assets/coworkings/logo-le-caré.png
---

> info "Un autre espace à Piton Saint-Leu"
> [Le CARé de Piton Saint-Leu]({% link _coworkings/41-le-caré-saint-leu.md %}) est un lieu du même groupe

Le CARé est un espace de coworking situé dans le quartier du Moufia à Saint-Denis.

## Équipements
Le site propose 5 postes de télétravail, 1 salle de réunion et 3 bureaux privatifs.

## Services
Des salles de locations sont disponibles à la location.

Il est également possible de faire domicilier son courrier.
