---
coarse_location: Saint-Louis
region: Sud
location:
  title: La Fabric by Adopt un toit
  latitude: -21.291717562734412
  longitude: 55.40614515030529
title: La Fabric by Adopt un toit
permalink: /coworking/saint-louis/la-fabric-by-adopt-un-toit/
website: https://www.lafabric.re/
phone: '+262 262 744 744'
email: contact@lafabric.re
address: 44 Avenue des Maldives, 97450 Saint-Louis
logo: assets/coworkings/logo-la-fabric.png
photo: assets/coworkings/la-fabric-by-adopt-un-toit-02.jpg
---
La Fabric est un lieu qui permet d'accueillir des coworkers et des réunions.

Il est situé à Saint-Louis, à proximité immédiate de la RN1.
<br><br>
<br><br>

## Services
{% picture assets/coworkings/la-fabric-by-adopt-un-toit-03.jpg width="250px" class="photo-float-right" alt="Open-space avec bureaux individuels" %}

Un open-space, des bureaux privatifs et des salles de réunion sont disponibles à la location.

## Espaces
Une grande salle de coworking permet d'y travailler sur des chaises ou même sur des banquettes.

Des bureaux individuels, disponibles à la location à la journée ou au mois, sont attenants à cette salle.

## Restauration
{% picture assets/coworkings/la-fabric-by-adopt-un-toit-03.jpg width="250px" class="photo-float-right" alt="Coin restauration" %}

Un grand espace permet de manger sur place. Un micro-onde et un frigidaire sont mis à disposition.

Un distributeur automatique de café et de friandises est présent.

## Accessibilité
Le site est accessible aux personnes à mobilité réduite.