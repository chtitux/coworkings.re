---
coarse_location: La Montagne, Saint-Denis
region: Nord
address:
phone: '+262 692 69 66 05'
email: maisondedith@gmail.com
address: 59 chemin Commins, 97417 La Montagne
location:
  latitude: -20.89000727768091
  longitude: 55.40717494313574
title: la Maison d'Edith
permalink: /coworking/saint-denis/la-maison-d-edith/
website: https://www.maisondedith.com/
---
La Maison d'Edith est un espace de réception pour séminaires, cocktails et conférences. Elle est située à La Montagne, dans les hauteurs de Saint-Denis.

Il est également possible d'y travailler de façon nomade.

## Services
### Hébergement sur place
Plusieurs chambres peuvent être louées sur place.

## Localisation et accessibilité
La Maison est située dans le quartier de Saint-Bernard, dans les hauteurs de Saint-Denis. On y accède par la route de la Montagne.

L'espace du Colorado, avec [le golf du Colorado](https://www.golfclubcolorado.fr/) est à 5 minutes de voiture.

### Restauration à proximité
L'offre de restauration extérieure n'est pas très développée. Plusieurs points de restauration rapide existent sur la RD41, route de la montagne, a 5 minutes en voiture.