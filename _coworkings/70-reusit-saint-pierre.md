---
coarse_location: Saint-Pierre
region: Sud
location:
  latitude: -21.317195092180913
  longitude: 55.47368605485669
title: Reusit Saint Pierre
permalink:  /coworking/saint-pierre/reusit/
website: https://www.reusit.re/
address: Angle de la route de la Z.I n°2, Rue Benoîte Boulard, 97410 Saint-Pierre
phone: '+262 262 25 10 54'
logo: assets/coworkings/logo-reusit.png
---
Association Réunion Situation est une couveuse d'entreprise.

Elle permet de louer des espaces de travail.

> info "Un autre espace à Saint-Benoît"
> [Réu.sit de Saint-Benoît]({% link _coworkings/91-reusit-saint-benoit.md %}) est un lieu du même groupe

