---
layout: page
title: Espaces naturels où travailler à la Réunion
menu_title: 🌱 Nature
permalink: /espaces-naturels-teletravail-reunion/
---
Ces lieux sont en pleine nature. Vous pouvez probablement vous y trouver une petite place pour vous mettre dans le plus beau cadre. Attention cependant : il est peu probable qu'une prise de courant soit disponible, ni un réseau Wifi. Prévoyez de charger votre batterie et votre partage de connexion.

Des kiosques sont généralement présents et permettent de travailler assis.

{% include list-places.html collection="espaces-naturels" %}

# Carte
{% google_map type='espace-naturel' width="100%" height="500" %}
