---
layout: page
title: À propos – Mentions légales
permalink: /à-propos/
---

# À propos
Ce site a pour but de référencer les lieux d'espaces de coworking et de travail partagés sur la Réunion.

**Il n'est affilié à aucun espace de travail en particulier**.

Si vous souhaitez faire figurer, modifier ou supprimer des informations de votre espace sur ce site, n'hésitez pas à nous contacter. Nous le ferons avec plaisir.

# License d'utilisation

> info "Le contenu de ce site est librement réutilisable, sous conditions"
> À l'exception des logos et des marques qui appartiennent à leurs propriétaires respectifs,
> **le contenu de ce site et les données sont disponibles sous la license [Creative Commons CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr)**.

Vous êtes libres de partager, reprendre et adapter son contenu. Il est cependant nécessaire de conserver l'attribution des données et d'ajouter une mention de ce site.

# API
L'intégralité des données peuvent être récupérées sous la forme d'un fichier [GeoJSON](https://geojson.org/).
L'adresse est
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code><a href="{{ site.url }}{% link 15-api-all.geojson %}">{{ site.url }}{% link 15-api-all.geojson %}</a></code></pre></div></div>

Un fichier ne contenant que les espaces de coworkings est également disponible :
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code><a href="{{ site.url }}{% link 15-api-coworkings.geojson %}">{{ site.url }}{% link 15-api-coworkings.geojson %}</a></code></pre></div></div>

Les données de l'API sont soumis à la même license que le reste du site.

# Code source
Le code source du site est hébergé sur Gitlab à l'adresse [https://gitlab.com/chtitux/coworkings.re](https://gitlab.com/chtitux/coworkings.re).
N'hésitez pas à y contribuer directement.

# Mentions légales
Ce site est édité par la société [SysDevRun](https://www.sys-dev-run.fr/). Théophile Helleboid en est le directeur de la publication.

SysDevRun est établie au 112 rue Pablo Neruda, 97419 La Possession, à la Réunion. Vous pouvez le contacter par email : `contact@coworkings.re`.
L'hébergeur du site est la société [Gitlab.com](https://about.gitlab.com/).

L'entreprise SysDevRun est une EURL au capital social de 4&nbsp;000&nbsp;€,
  immatriculée au Registre du Commerce et des Sociétés de Saint-Denis sous le numéro 900 998 014.

# Données personnelles

> info "Nous ne collectons pas de données personnelles."
> Et il n'y a aucune publicité

Il n'y a pas de compteur de visites, pas de collecte de données personnelles ni de *tracker* sur ce site.

Les cartes interactives sont affichées par le service [Google Maps Platform](https://developers.google.com/maps), qui ne dépose pas de cookies.

Si vous le souhaitez, vous pouvez tout de même joindre le responsable à la protection des données du site avec les coordonnées présentes sur cette page.
