---
layout: page
title: Espaces adaptés au télé-travail à la Réunion
menu_title: ☕️ Espaces adaptés
permalink: /espaces-adaptes-teletravail-reunion/
---
Ces lieux peuvent généralement vous accueillir pour une session de travail nomade. N'hésitez pas à les contacter à l'avance,
afin qu'ils vous confirment qu'une place est bien disponible.

{% include list-places.html collection="espaces-adaptes" %}

# Carte
{% google_map type='espace-adapte' width="100%" height="500" %}
