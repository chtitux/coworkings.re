---
layout: page
title: 📍 Carte
menu_title: 📍 Carte
permalink: /carte/
---
Cette carte rassemble ![](/assets/marker-red.svg) [les espaces de coworking]({% link 20-coworkings.markdown %}), ![](/assets/marker-blue.svg) [les espaces adaptés]({% link 30-espaces-adaptes.markdown %}) et ![](/assets/marker-green.svg) les [espaces naturels]({% link 40-espaces-naturels.markdown %}).

{% google_map place="true" width="100%" height="700" %}

<br/>

## Liste des espaces où travailler de façon nomade à la Réunion
### [Espaces de coworking]({% link 20-coworkings.markdown %})
<ul>
  {% for item in site["coworkings"] %}
    <li><a href="{{ item.url }}">{{ item.title }}</a>, {{ item.coarse_location }}</li>
  {% endfor %}
</ul>

### [Espaces adaptés]({% link 30-espaces-adaptes.markdown %})
<ul>
  {% for item in site["espaces-adaptes"] %}
    <li><a href="{{ item.url }}">{{ item.title }}</a>, {{ item.coarse_location }}</li>
  {% endfor %}
</ul>

### [Espaces naturels]({% link 40-espaces-naturels.markdown %})
<ul>
  {% for item in site["espaces-naturels"] %}
    <li><a href="{{ item.url }}">{{ item.title }}</a>, {{ item.coarse_location }}</li>
  {% endfor %}
</ul>