Intro.

## Équipements
- Table/chaises
- Prise électrique
- Wifi
- Cabine pour s'isoler

## Services
### Bureaux privatifs
### Salle de réunion
### Snack/Restauration sur place
### Domiciliation

## Prix et formules
- 1/2 journée, journée
- Abonnement

## Localisation et accessibilité
- Proximité de site touristiques
- Parking?

### Restauration à proximité