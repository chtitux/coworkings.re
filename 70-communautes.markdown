---
layout: page
title: Communautés d'indépendants et de coworkers à la Réunion
menu_title: 🤝 Communautés
permalink: /communautes-independants-freelances-reunion/
---

# [Bann' coworkers](https://banncoworkers.re/)
{% picture assets/communautes/logo-bann-coworkers.jpg width="150px" class="photo-float-right" alt="Logo Bann'Coworkers" %}

**Bann' coworkers** est une communauté d'indépendants et de travailleurs nomades à la Réunion. Ils sont organisés autour du groupe Facebook [BannCoworkers](https://www.facebook.com/BannCoworkers) et d'un groupe WhatsApp.

Le groupe organise régulièrement des rencontres ouvertes à toutes et tous dans des lieux adaptés aux télé-travailleurs, partout dans l'île.

Ils sont également à l'origine d'une [carte des espaces de coworking](https://banncoworkers.re/coworkmap/), qui a inspiré la création de ce site.

# [Makers.re](https://www.makers.re/)
{% picture assets/communautes/logo-makers.re.png width="150px" class="photo-float-right" alt="Logo makers.re" %}

**Makers.re** est une communauté d'entrepreneuses, d'entrepreneurs, de créatrices, de créateurs créée par [Julien Collet](https://www.linkedin.com/in/julien-collet/).

C'est une communauté pour inspirer et motiver les personnes basées à La Réunion qui veulent entreprendre localement ou mondialement.

Une newsletter hebdomadaire et plusieurs évènements par mois sont portés par Makers.re.

# [Le Crew](https://www.lecrew.re/)
{% picture assets/communautes/logo-le-crew.png width="150px" class="photo-float-right" alt="Logo Le Crew" %}

**Le Crew** est une communauté d’entrepreneurs animée par [Mathilde Fraisse](https://www.linkedin.com/in/mathilde-fraisse-64454460/). C'est également un 
[espace de coworking à la Saline les Bains]({% link _coworkings/35-le-crew.md %}).

De jeunes passionnés s’entraident au quotidien autour d’un groupe WhatsApp. Chaque semaine un évènement est organisé au sein de l’espace de coworking : un atelier avec un expert ou un afterwork avec une conférence.

En rejoignant Le Crew, vous pourrez améliorer votre projet, capter des opportunités et élargir votre réseau à la Réunion.