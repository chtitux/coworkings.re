---
coarse_location: Le Tampon
region: Sud
location:
  latitude: -21.190644182304936
  longitude: 55.53686742782477
title: Belvédère de Bois Court
permalink:  /espace-naturel/le-tampon/belvédère-de-bois-court/
address: Belvédère de Bois Court, 97430 Le Tampon
logo: assets/espaces-naturels/logo-ville-le-tampon.png
---
Le belvédère de Bois Court est un grand espace avec plusieurs kiosques.

Aucune prise électrique n'est présente.

