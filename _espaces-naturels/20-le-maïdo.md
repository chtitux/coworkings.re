---
coarse_location: Le Maïdo, Saint-Paul
region: Ouest
location:
  title: Le Maïdo
  latitude: -21.068575118409644
  longitude: 55.378186828190614
title: Le Maïdo
permalink:  /espace-naturel/saint-paul/le-maïdo/
logo: assets/espaces-naturels/logo-ville-saint-paul.png
address: Route du Maïdo, Saint-Paul
---
Le **Maïdo** est un site naturel exceptionnel. Il offre une vue magnifique sur le cirque de Mafate.

Il n'y a pas de lieu où s'asseoir au sommet du Maïdo, mais quelques kiosques
existent un peu plus bas.

Aucune prise électrique n'est disponible.

## Restauration
Il est possible de se restaurer au restaurant sur le site de la luge du Maïdo.

