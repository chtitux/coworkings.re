---
title: Le Colorado
permalink: /espace-naturel/saint-denis/colorado/
coarse_location: La Montagne, Saint-Denis
region: Nord
location:
  title: Le Colorado
  latitude: -20.9107278062345
  longitude: 55.42267474421622
logo: assets/espaces-naturels/logo-ville-saint-denis.png
---
Le **Colorado** est un grand site naturel surplombant Saint-Denis.

Plusieurs kiosques sont répartis dans le parc.

## Restauration
Le [restaurant du Colorado](https://www.facebook.com/restaurantlecolorado/) vous permettra de vous restaurer sur place.


