---
coarse_location: Saint-Pierre
region: Sud
location:
  latitude: -21.368853352097048
  longitude: 55.5472263505459
title: Plage de Grand Anse
permalink:  /espace-naturel/saint-pierre/plage-grand-anse/
address: Plage de Grand Anse, Petite Île
logo: assets/espaces-naturels/logo-ville-petite-ile.png
photo: assets/espaces-naturels/70-grand-anse-01.jpg
---
La plage de Grand Anse est une grande plage sur la commune de Petite Île.

Le cadre est magnifique et permet d'admirer l'Océan Indien.

Il n'y a pas de kiosque couvert, il faut être capable de travailler au grand air.

> warning "La baignade est interdite"
> Il n'est pas autorisé de se baigner sur la plage. Un arrêté municipal l'interdit pour prévenir les risques naturels liés au lieu.
## Restauration
Deux restaurants sont ouverts le midi sur place.
