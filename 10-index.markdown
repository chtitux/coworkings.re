---
layout: home
permalink: /
---

# Annuaire des espaces de coworking à la Réunion

{% google_map place="true" width="100%" height="470" %}

<br/>

## Espace de coworkings
Plusieurs [espaces de coworking existent à la Réunion]({% link 20-coworkings.markdown %}). Ces lieux sont toujours équipés pour le télé-travail, les *digital nomads* et les personnes cherchant un endroit professionnel pour travailler.


## Espaces de travail adaptés
Ces espaces ne sont pas des espaces de coworking à proprement parler, mais offrent généralement tout le confort pour y travailler. Consultez la page [espaces adaptés au télé-travail à la Réunion]({% link 30-espaces-adaptes.markdown %}).


## Espace naturels adaptés
Les [espaces naturels où il est possible de travailler à la Réunion]({% link 40-espaces-naturels.markdown %}) existent ! Il n'y aura pas autant de confort que dans des bâtiments, mais les paysages vous raviront.

## Groupes et communautés
Plusieurs [groupes d'entraides entre coworkers]({% link 70-communautes.markdown %}) existent à la Réunion.
De nombreux évènements, généralement ouverts à toutes et tous, sont organisés par ces communautés. N'hésitez pas à rentrer en contact avec eux, il y a sûrement un groupe qui vous conviendra !
