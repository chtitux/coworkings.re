---
layout: page
title: Espaces de coworking à la Réunion
menu_title: 🏢 Coworkings
permalink: /espaces-coworking-reunion/
---
Plusieurs espaces sont gérés en tant qu'espace de coworkings à la Réunion.
L'espace est configuré pour un espace dédié aux travailleurs nomades.
Des prises électriques et une connectivité Internet y sont systématiques.

{% include list-places.html collection="coworkings" %}

# Carte
{% google_map type='coworking' width="100%" height="500"  %}
