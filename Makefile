serve:
	bundle exec jekyll serve --livereload

build:
	bundle exec jekyll build

build-prod:
	JEKYLL_ENV=production bundle exec jekyll build

install:
	bundle install
