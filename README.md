# [coworkings.re](https://www.coworkings.re/)

## Setup
Requirements:
* Ruby 3.1.2

Install gems with:
```console
make install
```

## Serve
```console
make serve
```

## Contribution
Feel free to open a Merge Request and suggest improvements and new locations to this website!

Have in mind the site in under [Creative Commons CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.


## License
[Creative Commons CC BY 4.0](LICENSE).
